/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef LOADBAR_H
#define LOADBAR_H

#include "widget.h"
#include "objects.h"

namespace jwl{
	class LoadBar : public Widget
	{
		private:
			sf::RectangleShape fg, bg;
			sf::VertexArray outline;
			sf::Text text;
		protected:
			float percentage;
		public:
			LoadBar();
			LoadBar(sf::Vector2f, sf::Vector2f, sf::Font*, unsigned int, ColorPair);
			/**
			 * Percentage tussen 0.0000 en 1.0000. \n
			 */

			void updatePct(float);

			virtual void Refresh();
	};
}

#endif
