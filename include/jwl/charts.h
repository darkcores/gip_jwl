/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef CHARTS_H
#define CHARTS_H

#include "widget.h"

namespace jwl {
	namespace charts {
		class Base : public Widget {
			public:
				Base(sf::Vector2f, sf::Vector2f, sf::Font*, unsigned int, ColorPair);
				~Base();
		};
		class Histogram : public Base {
			public:
				Histogram(sf::Vector2f, sf::Vector2f, sf::Font*, unsigned int, ColorPair);
				~Histogram();
		};
		class FreqPoly : public Base {
			public:
				FreqPoly(sf::Vector2f, sf::Vector2f, sf::Font*, unsigned int, ColorPair);
				~FreqPoly();
		};
		class Scatter : public Base {
			public:
				Scatter(sf::Vector2f, sf::Vector2f, sf::Font*, unsigned int, ColorPair);
				~Scatter();
		};
		class Regression : public Base {
			public:
				Regression(sf::Vector2f, sf::Vector2f, sf::Font*, unsigned int, ColorPair);
				~Regression();
		};
	}
}

#endif
