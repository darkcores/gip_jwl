/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef OBJECTS_H
#define OBJECTS_H

#include <SFML/Graphics.hpp>

namespace jwl{
	/**
	 * Soorten events op een object/widget . \n
	 */
	enum Events{
		Click,
		Char,
		Hover,
		Up,
		Down,
		Left,
		Right,
		Next,
		Previous
	};

	/**
	 * Klasse voor kleurenpaar. \n
	 * Deze klasse kan meerdere keren gebruikt worden en is enkel lokaal.
	 */
	class ColorPair{
		public:
			/**
			 * Constructor voor de standaard kleuren in te stellen. \n
			 */
			ColorPair();
			/**
			 * Constructor voor beide kleuren direct in te stellen. \n
			 */
			ColorPair(sf::Color, sf::Color);
			/**
			 * Eerste kleur. \n
			 */
			sf::Color Primary;
			/**
			 * Tweede kleur. \n
			 */
			sf::Color Secondary;
			/**
			 * Geeft inverted kleurenpaar terug. \n
			 */
			ColorPair inverted();
	};

	/**
	 * Klasse met de globale kleuren van het programma. \n
	 * Deze klasse heeft static variabelen dus de variabelen zijn overal hetzelfde.
	 */
	class ColorPair_s{
		public:
			/**
			 * Constructor voor standaard kleur in te stellen. \n
			 */
			ColorPair_s();
			/**
			 * Constructor voor beide kleuren direct in te stellen. \n
			 */
			ColorPair_s(sf::Color, sf::Color);
			/**
			 * Eerste globale kleur. \n
			 */
			static sf::Color Primary_s;
			/**
			 * Tweede globale kleur. \n
			 */
			static sf::Color Secondary_s;
			/**
			 * Geef een gewoon ColorPair terug. \n
			 */
			static ColorPair Colors();
			/**
			 * Geef een inverted kleurenpaar terug. \n
			 */
			static ColorPair inverted();
	};


	/**
	 * Klasse met pointers naar object voor interactie. \n
	 * Bevat informatie over grootte, positie en een pointer naar het object.
	 */
	class Action{
		public:
			/**
			 * Constructor met nul waarden. \n
			 */
			Action();
			/**
			 * Constructor met alle variabelen. \n
			 */
			Action(sf::Vector2f *, sf::Vector2f *, void *);
			/**
			 * Grootte van het object. \n
			 */
			sf::Vector2f *size;
			/**
			 * Positie van het object. \n
			 */
			sf::Vector2f *position;
			/**
			 * Pointer naar het object.\n
			 * Void object zodat bepaalde objecten andere functies kunnen oproepen.
			 */
			void *object;
	};

	class VideoSettings{
		public:
			/**
			 * Contructor, maakt standaard resolutie van sf::getVideoMode(). \n
			 */
			VideoSettings();

			/**
			 * Videomode met de resolutie. \n
	 		 */
			static sf::VideoMode Resolution;

			/**
			 * Vector met schaal. \n
			 */
			static sf::Vector2f Scale;
	};
}

#endif
