/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef WIDGET_H
#define WIDGET_H

#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>

#include "objects.h"

namespace jwl{
	/**
	 * De basis klasse voor alle widgets. \n
	 * Alle widgets bouwen op deze klasse.
	 */
	class Widget : public sf::Drawable{
		private:
			virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const
			{
				(void)states;
				for(unsigned int i = 0; i < Background.size(); i++)
				{
					target.draw(*Background[i]);
				}
				for(unsigned int i = 0; i < Foreground.size(); i++)
				{
					target.draw(*Foreground[i]);
				}
			}
		public:
			/**
			 * Achtergrond objecten. \n
			 */
			std::vector<sf::Drawable *> Background;
			/**
			 * Voorgrond objecten. \n
			 */
			std::vector<sf::Drawable *> Foreground;
			/**
			 * Kleurenpaar. \n
			 * Dit is een nullptr als niet gebruikt wordt.
			 */
			ColorPair Colors;
			/**
			 * Grootte van het object. \n
			 */
			sf::Vector2f Size;
			/**
			 * Positie van het object. \n
			 */
			sf::Vector2f Position;
			/**
			 * Pointer naar font. \n
			 */
			sf::Font *Font;
			/**
			 * Grootte van het font. \n
			 */
			unsigned int FontSize;

			/**
			 * Constructor van de klasse. \n
			 */
			Widget();
			/**
			 * Constructor met grootte, positie en font. \n
			 */
			Widget(sf::Vector2f, sf::Vector2f, sf::Font*, unsigned int);
			/**
			 * Constructor met grootte, positie, font en kleurenpaar. \n
			 */
			Widget(sf::Vector2f, sf::Vector2f, sf::Font*, unsigned int, ColorPair);
			
			/**
			 * Event behandelen. \n
			 * Geeft event type en parameter pointer door.
			 */
			virtual void handleEvent(Events, void *);
			/**
			 * Hernieuwt de grootte, kleur, etc ...
			 */
			virtual void Refresh();

			/**************************************************/
		private:
			sf::Vector2f startvect, destvect;
			ColorPair destcolor;
			bool move, color;
			sf::Vector2f lerp_pct(float);
			ColorPair fade_pct(float);
		public:
			void MoveSet(sf::Vector2f, sf::Vector2f);
			void ColorSet(ColorPair);
			void Effect(float);
			/**************************************************/

			/**
			 * Geeft Action object terug. \n
			 */
			Action getAction();
	};
}

#endif
