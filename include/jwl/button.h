/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef BUTTON_H
#define BUTTON_H

#include <SFML/Graphics.hpp>

#include "objects.h"
#include "label.h"

namespace jwl{
	/**
	 * Deze class is een button object voor knoppen te maken. \n
	 */	
	class Button : public Label
	{
		private:
			sf::VertexArray border;
		public:
			/**
			 * De constructor voor initialisatie. \n
			 */
			Button();
			/**
			 * Constructor met label, font, grootte en positie. \n
			 */
			Button(std::string, sf::Font*, unsigned int, sf::Vector2f, sf::Vector2f);
			/**
			 * Constructor met label, font, grootte, positie en kleurenpaar. \n
			 */
			Button(std::string, sf::Font*, unsigned int, sf::Vector2f, sf::Vector2f, ColorPair);
	
			virtual void Refresh();
	};
}

#endif
