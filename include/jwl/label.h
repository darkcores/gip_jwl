/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef LABEL_H
#define LABEL_H

#include "objects.h"
#include "widget.h"

namespace jwl{
	/**
	 * Deze class is een object om labels te maken met tekst. \n
	 */
	class Label : public Widget
	{
		private:
			sf::RectangleShape back;
		public:
			sf::Text Text;
		public:
			/**
			 * Constructor van de class. \n
			 */
			Label();
			/**
			 * Constructor met label, font, grootte en positie
			 */
			Label(std::string, sf::Font*, unsigned int, sf::Vector2f, sf::Vector2f);
			/**
			 * Constructor met label, font, grootte, positie en kleuren
			 */
			Label(std::string, sf::Font*, unsigned int, sf::Vector2f, sf::Vector2f, ColorPair);

			/**
			 * String met het label. \n
			 */
			std::string labeltxt;

			virtual void Refresh();
	};

}

#endif
