/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef FILEDIALOG_H
#define FILEDIALOG_H

#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <SFML/Graphics.hpp>
#include "messagebox.h"
#include "table.h"

namespace jwl {
	class Filedialog : public Messagebox
	{
		private:
			Table *tbl;
		public:
			/**
			 * Constuctor voor initalisatie. \n
			 */
			Filedialog();
			Filedialog(sf::Font *, unsigned int, ColorPair);
			/**
			 * Destructor van de class. \n
			 */
			~Filedialog();

			/**
			 * String met file path
			 */
			std::string path;

			void loadPath();

			virtual void Refresh();
			virtual void Click();
	};
}

#endif
