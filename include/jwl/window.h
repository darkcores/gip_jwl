/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef WINDOW_H
#define WINDOW_H

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <vector>
#include <thread>
#include <atomic>

#include "objects.h"
#include "widget.h"

/**
 * Namespace voor het framework. \n
 * Deze namespace bevat alle classes en functies.
 */
namespace jwl{
	/**
	 * Deze class omvat alle objecten. \n
	 * Tekent deze objecten en verwerkt events.
	 */
	class Window : public sf::Drawable {
		private:
			virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const
			{
				(void)states;
				for(unsigned int i = 0; i < Background.size(); i++)
				{
					target.draw(*Background[i]);
				}
				for(unsigned int i = 0; i < Foreground.size(); i++)
				{
					target.draw(*Foreground[i]);
				}
				for(unsigned int i = 0; i < Overlay.size(); i++)
				{
					target.draw(*Overlay[i]);
				}
			}
			int testAction(std::vector<Action> &, sf::Vector2f &);
			int testHover(std::vector<Action> &, sf::Vector2i &);
			sf::RectangleShape overlay;
			sf::VertexArray border;
		public:
			/**
			 * Constructor. \n
			 */
			Window();
			/**
			 * Constructor met font en fontsize. \n
			 */
			Window(sf::Font*, unsigned int);
			/**
			 * Constructor met font en kleurenpaar. \n
			 */
			Window(sf::Font *, unsigned int, ColorPair);
			
			/**
			 * Window pointer. \n
			 */
			sf::RenderWindow *win;
			/**
			 * Pointer naar het geladen font. \n
			 */
			sf::Font *Font;
			/**
			 * Grootte van het font. \n
			 */
			unsigned int FontSize;
			/**
			 * Vector met de achtergond objecten. \n
			 */
			std::vector<sf::Drawable *> Background;
			/**
			 * Vector met voorgrond objecten. \n
			 */
			std::vector<sf::Drawable *> Foreground;
			/**
			 * Vector met overlay elementen
			 */
			std::vector<sf::Drawable *> Overlay;
			/**
			 * Kleurenpaar voor de items. \n
			 */
			ColorPair Colors;
			/**
			 * Vector met alle objecten voor hovers. \n
			 */
			std::vector<Action> Hover;
			/**
			 * Vector met alle acties voor events. \n
			 */
			std::vector<Action> Actions;
			/**
			 * Bool voor de main thread. \n
			 */
			std::atomic<bool> isRunning;
			/**
			 * Het geselecteerde item. \n
			 */
			unsigned int selected;
			
			/**
			 * Verfrist alle objecten. \n
			 * Hier moet je alle objecten in tekenen, font geven etc...
			 */
			virtual void Refresh();
			/**
			 * Functie bij starten van venster. \n
			 * Bijvoorbeeld voor animaties in te zetten.
			 */
			virtual void LoadFunc();
			/**
			 * Functie bij einde van het venster. \n
			 */
			virtual void EndFunc();
			/**
			 * "Run" het venster/window. \n
			 */
			void Run(sf::RenderWindow*);
			/**
			 * Zet de hover op geselecteerd object. \n
			 */
			void SetHover();
			/**
			 * Zet border op geselecteerd object. \n
			 */
			void SetBorder();
			/**
			 * Functie voor handelingen bij muisklik. \n
			 */
			virtual void Click();
	};
}

#endif
