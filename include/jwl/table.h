/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef TABLE_H
#define TABLE_H

#include <SFML/Graphics.hpp>
#include "objects.h"
#include "widget.h"
#include <ctype.h>
#include <string>
#include <vector>
#include <sqlite3.h>

namespace jwl{
	/**
	 * Object om tabellen to maken. \n
	 * Tabellen kunnen meerdere kolommen hebben \n
	 * Of je kan een SQLite3 database bestand als bron gebruiken
	 */
	class Table : public Widget
	{
		public:
			sqlite3 *db;
			sf::VertexArray grid;
			sf::RectangleShape back, sbar , sslider, overlay;
			
			std::vector< std::vector< std::string > > lst;
			std::vector< sf::Text > txtlst;

			unsigned int startlst;
			void generateGrid();
			void updateText();
			void updateScrollbar();
			void updateOverlay();
			bool listmode;
			float *widths;
			bool custom_width;
		public:
			/**
			 * Constructor. \n
			 */
			Table();
			Table(sf::Vector2i, sf::Font *, unsigned int,
					sf::Vector2f, sf::Vector2f, ColorPair);
			Table(int, sf::Font *, unsigned int,
					sf::Vector2f, sf::Vector2f, ColorPair, bool);

			/**
			 * Het geselecteerde item. \n
			 */
			unsigned int selected;
			/**
			 * Breedte van de kolommen. \n
			 * Leeglaten indien automatisch.
			 */
			std::vector< unsigned int > colwidth;
			/**
			 * Aantal kolommen en rijen zichtbaar. \n
			 */
			sf::Vector2i tablesize;
			/**
			 * Sneller refresh met alleen text veranderen. \n
			 */
			void QuickRefresh();
			/**
			 * Maak tabel leeg en zet alles op 0. \n
			 */
			void clear();

			void setWidth(unsigned char, float);

			virtual void Refresh();
			virtual void handleEvent(Events, void*);

			/**
			 * Zet een database om data te laden uit een sqlite3 database. \n
			 */
			void setDataSource(const char*, const char*);
			/**
			 * Callback functie voor de database. \n
			 */
			int callback(int, char **, char **);
	};
}

#endif
