/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef MBOX_H
#define MBOX_H

#include <SFML/Graphics.hpp>

#include "objects.h"
#include "window.h"
#include "button.h"

namespace jwl{
/**
 * Class voor een messagebox met een OK en CANCEL knop. \n
 */
	class Messagebox : public Window
	{
		private:
			sf::VertexArray border;
		public:
			/**
			 * Overlay over het hele scherm. \n
			 */
			sf::RectangleShape bgoverlay;
			/**
			 * achtergrond van het object. \n
			 */
			sf::RectangleShape back;
			/**
			 * De tekst van het object. \n
			 */
			sf::Text Text;
			/**
			 * De OK knop. \n
			 */
			Button ok;
			/**
			 * De CANCEL knop. \n
			 */
			Button cancel;
			
			/**
			 * De constructor van de class voor initialisatie. \n
			 */
			Messagebox();
			/**
			 * Constructor met font, fontsize. \n
			 */
			Messagebox(sf::Font*, unsigned int);
			/**
			 * Constructor met font, fontsize, colors
			 */
			Messagebox(sf::Font*, unsigned int, ColorPair);
		
			/**
			 * Boolean met het resultaat. \n
			 * OK = true <-> CANCEL = false
			 */
			bool result;
			
			virtual void Refresh();
			virtual void Click();
	};
}

#endif
