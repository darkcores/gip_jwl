/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef TEXTBOX_H
#define TEXTBOX_H

#include <SFML/Graphics.hpp>

#include "objects.h"
#include "label.h"

namespace jwl{
	/**
	 * Class voor een textbox object om tekst/wachtwoorden in te voeren. \n
	 */	
	class Textbox : public Label
	{
		public:
			/**
			 * String met sterretjes. \n
			 * De letters staan in de string uit "Label".
			 */
			std::string stars;
			/**
			 * Het maximum aantal characters. \n
			 */
			unsigned int maxlen;
	
			/**
			 * De constructor van de class voor initialisatie. \n
			 */
			Textbox();
			/**
			 * Constructor met font, fontsize, grootte en locatie. \n
			 */
			Textbox(sf::Font*, unsigned int, sf::Vector2f, sf::Vector2f);
			/**
			 * Constructor met font, fontsize, grootte, locatie en kleurenpaar. \n
			 */
			Textbox(sf::Font*, unsigned int, sf::Vector2f, sf::Vector2f, ColorPair);
		
			virtual void handleEvent(Events, void *);	
	};
}

#endif

