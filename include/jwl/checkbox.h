/* inherit van jwl::button en voeg checkbox toe */

/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef CHECKBOX_H
#define CHECKBOX_H

#include "button.h"

namespace jwl{
	class Checkbox : public Button
	{
		private:
			sf::VertexArray box, tick;
		public:
			Checkbox();
			Checkbox(std::string, sf::Font*, unsigned int,
					sf::Vector2f, sf::Vector2f, ColorPair, bool);
			void set(bool);
			bool isChecked;

			virtual void Refresh();
			virtual void handleEvent(Events, void*);
	};
}

#endif
