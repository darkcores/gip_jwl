/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "../include/jwl/inputbox.h"

namespace jwl{
	Inputbox::Inputbox(sf::Font *font, unsigned int fontsize, jwl::ColorPair colors)
		: Messagebox::Messagebox(font, fontsize, colors)
	{
		Background.push_back(&input);
		Refresh();
	}
	void Inputbox::Refresh()
	{
		Messagebox::Refresh();
		input.Font = Font;
		input.FontSize = FontSize;
		input.Size = sf::Vector2f(VideoSettings::Scale.x * 9, VideoSettings::Scale.y);
		input.Position = sf::Vector2f(9 * VideoSettings::Scale.x, VideoSettings::Scale.y * 11);
		input.Colors = Colors.inverted();
		input.Refresh();
		Actions.push_back(input.getAction());
	}
}
