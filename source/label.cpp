/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "../include/jwl/label.h"

namespace jwl{
	Label::Label() : Widget::Widget()
	{
		Background.push_back(&back);
		Foreground.push_back(&Text);
	}
	Label::Label(std::string text, sf::Font *font, unsigned int fontsize,
			sf::Vector2f size, sf::Vector2f position)
		: Widget::Widget(size, position, font, fontsize)
	{
		Background.push_back(&back);
		Foreground.push_back(&Text);
		labeltxt = text;
		Refresh();
	}
	Label::Label(std::string text, sf::Font *font, unsigned int fontsize,
			sf::Vector2f size, sf::Vector2f position, ColorPair colors)
		: Widget::Widget(size, position, font, fontsize, colors)
	{
		Background.push_back(&back);
		Foreground.push_back(&Text);
		labeltxt = text;
		Refresh();
	}
	void Label::Refresh()
	{
		Text.setColor(Colors.Primary);
		Text.setFont(*Font);
		Text.setCharacterSize(FontSize);
		Text.setString(labeltxt);
		back.setFillColor(Colors.Secondary);
		back.setPosition(Position.x, Position.y);
		Text.setPosition(Position.x + 10, Position.y + 10);
		back.setSize(Size);
	}
}
