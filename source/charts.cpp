/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "../include/jwl/charts.h"

namespace jwl { namespace charts {
	Base::Base(sf::Vector2f size, sf::Vector2f position, sf::Font *font,
			unsigned int fontsize, ColorPair colors)
		: Widget::Widget(size, position, font, fontsize, colors)
	{
	}
	Base::~Base()
	{
	}
	Histogram::Histogram(sf::Vector2f size, sf::Vector2f position, sf::Font *font,
			unsigned int fontsize, ColorPair colors)
		: Base::Base(size, position, font, fontsize, colors)
	{
	}
	Histogram::~Histogram()
	{
	}
	FreqPoly::FreqPoly(sf::Vector2f size, sf::Vector2f position, sf::Font *font,
			unsigned int fontsize, ColorPair colors)
		: Base::Base(size, position, font, fontsize, colors)
	{
	}
	FreqPoly::~FreqPoly()
	{
	}
	Scatter::Scatter(sf::Vector2f size, sf::Vector2f position, sf::Font *font,
			unsigned int fontsize, ColorPair colors)
		: Base::Base(size, position, font, fontsize, colors)
	{
	}
	Scatter::~Scatter()
	{
	}
	Regression::Regression(sf::Vector2f size, sf::Vector2f position, sf::Font *font, 
			unsigned int fontsize, ColorPair colors)
		: Base::Base(size, position, font, fontsize, colors)
	{
	}
	Regression::~Regression()
	{
	}
}}
