/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "../include/jwl/textbox.h"

namespace jwl{
	Textbox::Textbox()
		: Label::Label()
	{
		maxlen = 255;
	}
	Textbox::Textbox(sf::Font *font, unsigned int fontsize,
			sf::Vector2f size, sf::Vector2f position)
		: Label::Label("", font, fontsize, size, position)
	{
		maxlen = 255;
	}
	Textbox::Textbox(sf::Font *font, unsigned int fontsize,
			sf::Vector2f size, sf::Vector2f position, ColorPair colors)
		: Label::Label("", font, fontsize, size, position, colors)
	{
		maxlen = 255;
	}
	void Textbox::handleEvent(Events e, void *param)
	{
		switch (e)
		{
			{
				case Events::Char:
					char *c = reinterpret_cast<char*>(param);
					if(*c != '\b' && labeltxt.size() <= maxlen)
					{
						labeltxt += c;
					}
					else if(*c == '\b' && labeltxt.size() > 0)
					{
						labeltxt.pop_back();
					}
					Refresh();
					break;
			}
			default:
				break;
		}
	}
}
