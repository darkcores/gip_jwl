/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "../include/jwl/checkbox.h"

namespace jwl{
	Checkbox::Checkbox()
		: Button::Button()
	{
		Foreground.push_back(&box);
		Foreground.push_back(&tick);
		box.setPrimitiveType(sf::LinesStrip);
		tick.setPrimitiveType(sf::TrianglesStrip);
	}
	Checkbox::Checkbox(std::string text, sf::Font *font, unsigned int fontsize,
			sf::Vector2f size, sf::Vector2f position, ColorPair colors, bool b)
		: Button::Button(text, font, fontsize, size, position, colors)
	{
		Foreground.push_back(&box);
		Foreground.push_back(&tick);
		box.setPrimitiveType(sf::LinesStrip);
		tick.setPrimitiveType(sf::TrianglesStrip);
		isChecked = b;
		Refresh();
	}
	void Checkbox::set(bool b)
	{
		isChecked = b;
		sf::Color t;
		if(isChecked)
			t = Colors.Primary;
		else
			t = Colors.Secondary;
		for (uint8_t i = 0; i < 4; i++)
		{
			tick[i].color = t;
		}
	}
	void Checkbox::Refresh()
	{
		Button::Refresh();
		Text.setPosition(Position.x + Size.y, Position.y + 10);
		//draw box
		sf::Vector2f p = sf::Vector2f(Position.x + 5, Position.y + 5);
		box.resize(5);
		for (uint8_t i = 0; i < 5; i++)
		{
			box[i].color = Colors.Primary;
		}
		box[0] = p;
		box[1] = sf::Vector2f(p.x + Size.y - 10, p.y);
		box[2] = sf::Vector2f(p.x + Size.y - 10, p.y + Size.y - 10);
		box[3] = sf::Vector2f(p.x, p.y + Size.y - 10);
		box[4] = p;

		//draw tick
		tick.resize(4);
		for (uint8_t i = 0; i < 4; i++)
		{
			tick[i].color = Colors.Primary;
		}
		tick[0] = sf::Vector2f(p.x, p.y + Size.y / 2.25);
		tick[1] = sf::Vector2f(p.x + Size.y / 5 * 2.25, p.y + Size.y - 10);
		tick[2] = sf::Vector2f(p.x + Size.y / 5 * 2, p.y + Size.y / 5 * 3);
		tick[3] = sf::Vector2f(p.x + Size.y - 10, p.y);
	}
	void Checkbox::handleEvent(Events e, void *params)
	{
		(void) params;
		switch(e)
		{
			case Events::Click:
				set(!isChecked);
				break;
			default:
				break;
		}
	}
}
