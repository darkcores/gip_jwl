/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
#include "../include/jwl/button.h"

namespace jwl{
	Button::Button()
		: Label::Label()
	{
		Foreground.push_back(&border);
		border.setPrimitiveType(sf::PrimitiveType::LinesStrip);
		border.resize(5);
	}
	Button::Button(std::string text, sf::Font *font, unsigned int fontsize,
			sf::Vector2f size, sf::Vector2f position)
		: Label::Label(text, font, fontsize, size, position)
	{
		Foreground.push_back(&border);
		border.setPrimitiveType(sf::PrimitiveType::LinesStrip);
		border.resize(5);
		Refresh();
	}
	Button::Button(std::string text, sf::Font *font, unsigned int fontsize,
			sf::Vector2f size, sf::Vector2f position, ColorPair colors)
		: Label::Label(text, font, fontsize, size, position, colors)
	{
		Foreground.push_back(&border);
		border.setPrimitiveType(sf::PrimitiveType::LinesStrip);
		border.resize(5);
		Refresh();
	}
	void Button::Refresh()
	{
		Label::Refresh();
		for(unsigned int i = 0; i < 5; i++)
		{
			border[i].color = Colors.Primary;
		}
		border[0].position = sf::Vector2f(Position.x + 2, Position.y + 2);
		border[1].position = sf::Vector2f(Position.x + Size.x - 2, Position.y + 2);
		border[2].position = sf::Vector2f(Position.x + Size.x - 2, Position.y + Size.y - 2);
		border[3].position = sf::Vector2f(Position.x + 2, Position.y + Size.y - 2);
		border[4].position = border[0].position;
	}
}
