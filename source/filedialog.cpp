/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "../include/jwl/filedialog.h"

namespace jwl {
	/**
	 * Seperator voor bestand ('/' en '\' voor *nix en dos)
	 */
	#ifdef _WIN32
		const char seperator = '\\';
		const uint8_t prefix_length = 3;
	#else
		const char seperator = '/';
		const uint8_t prefix_length = 1;
	#endif

	enum {
		OK,
		CANCEL
	};
	
	Filedialog::Filedialog()
		: Messagebox::Messagebox()
	{
		//Foreground.push_back(tbl);
		char pwd[255];
		getcwd(pwd, 255);
		path = pwd;
	}

	Filedialog::Filedialog(sf::Font *font, unsigned int fontsize, ColorPair colors)
		: Messagebox::Messagebox(font, fontsize, colors)
	{
		//Foreground.push_back(tbl); //geen idee waarom dit zou crashen
		char pwd[255];
		getcwd(pwd, 255);
		path = pwd;
		Refresh();
	}

	Filedialog::~Filedialog()
	{
		delete tbl;
	}

	void Filedialog::Refresh()
	{
		Messagebox::Refresh();
		tbl = new Table(sf::Vector2i(2, 12), Font, FontSize,
				sf::Vector2f(11*VideoSettings::Scale.x, 7*VideoSettings::Scale.y),
				sf::Vector2f(9*VideoSettings::Scale.x, 6*VideoSettings::Scale.y),
				Colors.inverted());
		tbl->setWidth(0, VideoSettings::Scale.x*8);
		tbl->setWidth(1, VideoSettings::Scale.x*3 - 10);
		Foreground.push_back(tbl);
		Actions.push_back(tbl->getAction());
		loadPath();
	}

	void Filedialog::loadPath()
	{
		Text.setString(path);
		tbl->clear();
		std::vector<std::string> item(2);
		item[0] = "Filename";
		item[1] = "Type";
		tbl->lst.push_back(item);
		if(path.size() > prefix_length)
		{
			item[0] = "..";
			item[1] = "Folder";
			tbl->lst.push_back(item);
		}
		struct dirent *i;
		DIR *dp = NULL;
		dp = opendir(path.c_str());
		if(dp == NULL)
		{
			std::cout << "Filedialog: Could not open directory\n";
			return;
		}
		while ((i = readdir(dp)))
		{
			if(i->d_name[0] != '.')
			{
				item[0] = i->d_name;
				struct stat st;
				stat(i->d_name, &st);
				if(S_ISDIR(st.st_mode))
					item[1] = "Folder";
				else
					item[1] = "File";
				tbl->lst.push_back(item);
			}
		}
		closedir(dp);
		tbl->QuickRefresh();
	}

	void Filedialog::Click()
	{
		switch(selected)
		{
			case OK:
					switch (tbl->selected)
					{
						case 1:
							if(path.size() > prefix_length)
							{
								int i = path.length() - 1;
								do {
									path.pop_back();
									i--;
								} while(path[i] != seperator && i > prefix_length - 1);
							}
							loadPath();
							break;
						default:
							path += seperator;
							path += tbl->lst[tbl->selected][0];
							if (strcmp(tbl->lst[tbl->selected][1].c_str(), "File") == 0)
							{
								isRunning = false;
								result = true;
							}
							else
								loadPath();
							break;
					}
				break;
			case CANCEL:
					isRunning = false;
					result = false;
				break;
			default:
				break;
		}
	}
}
