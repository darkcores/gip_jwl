/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "../include/jwl/messagebox.h"

namespace jwl{
	enum {
		OK,
		CANCEL
	};
	Messagebox::Messagebox()
		: Window::Window()
	{
		Background.push_back(&bgoverlay);
		Background.push_back(&back);
		Background.push_back(&Text);
		Background.push_back(&ok);
		Background.push_back(&cancel);
		Background.push_back(&border);
		border.setPrimitiveType(sf::PrimitiveType::LinesStrip);
	}
	Messagebox::Messagebox(sf::Font *font, unsigned int fontsize)
		: Window::Window(font, fontsize)
	{
		Background.push_back(&bgoverlay);
		Background.push_back(&back);
		Background.push_back(&Text);
		Background.push_back(&ok);
		Background.push_back(&cancel);
		Background.push_back(&border);
		border.setPrimitiveType(sf::PrimitiveType::LinesStrip);
		Refresh();
	}
	Messagebox::Messagebox(sf::Font *font, unsigned int fontsize, ColorPair colors)
		: Window::Window(font, fontsize, colors)
	{
		Background.push_back(&bgoverlay);
		Background.push_back(&back);
		Background.push_back(&Text);
		Background.push_back(&ok);
		Background.push_back(&cancel);
		Background.push_back(&border);
		border.setPrimitiveType(sf::PrimitiveType::LinesStrip);
		Refresh();
	}
	void Messagebox::Refresh()
	{
		border.resize(5);
		for (unsigned int i = 0; i <5; i++)
		{
			border[i].color = Colors.Primary;
		}
		border[0].position = sf::Vector2f(8*VideoSettings::Scale.x+2,4.5*VideoSettings::Scale.y+2);
		border[1].position = sf::Vector2f(24*VideoSettings::Scale.x-2,4.5*VideoSettings::Scale.y+2);
		border[2].position = sf::Vector2f(24*VideoSettings::Scale.x-2,13.5*VideoSettings::Scale.y-2);
		border[3].position = sf::Vector2f(8*VideoSettings::Scale.x+2,13.5*VideoSettings::Scale.y-2);
		border[4].position = border[0].position;
		bgoverlay.setFillColor(sf::Color(128, 128, 128, 192));
		back.setFillColor(Colors.Secondary);
		Text.setColor(Colors.Primary);
		bgoverlay.setFillColor(sf::Color(128, 128, 128, 64));
		bgoverlay.setSize(sf::Vector2f(32 * VideoSettings::Scale.x, 18 * VideoSettings::Scale.y));
		bgoverlay.setPosition(0, 0);
		back.setSize(sf::Vector2f(16 * VideoSettings::Scale.x, 9 * VideoSettings::Scale.y));
		back.setPosition(8 * VideoSettings::Scale.x, 4.5 * VideoSettings::Scale.y);
		Text.setPosition(9 * VideoSettings::Scale.x, 5.5 * VideoSettings::Scale.y);
		Text.setFont(*Font);
		Text.setCharacterSize(FontSize);
		ok.Font = Font;
		ok.FontSize = FontSize;
		ok.Size = sf::Vector2f(VideoSettings::Scale.x * 3, VideoSettings::Scale.y);
		ok.Position = sf::Vector2f(20.5 * VideoSettings::Scale.x, VideoSettings::Scale.y * 11);
		ok.Colors = Colors;
		ok.labeltxt = "Ok";
		cancel.Font = Font;
		cancel.FontSize = FontSize;
		cancel.Size = sf::Vector2f(VideoSettings::Scale.x * 3, VideoSettings::Scale.y);
		cancel.Position = sf::Vector2f(20.5 * VideoSettings::Scale.x, VideoSettings::Scale.y*12);
		cancel.Colors = Colors;
		cancel.labeltxt = "Annuleren";
		ok.Refresh();
		cancel.Refresh();
		Actions.push_back(ok.getAction());
		Actions.push_back(cancel.getAction());
		Hover.push_back(ok.getAction());
		Hover.push_back(cancel.getAction());
	}
	void Messagebox::Click()
	{
		switch(selected)
		{
			case OK:
				isRunning = false;
				result = true;
				break;
			case CANCEL:
				isRunning = false;
				result = false;
				break;
			default:
				break;
		}
	}
}
