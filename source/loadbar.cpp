/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "../include/jwl/loadbar.h"

namespace jwl{
	LoadBar::LoadBar()
		: Widget::Widget()
	{
		Background.push_back(&bg);
		Foreground.push_back(&outline);
		Foreground.push_back(&fg);
		Foreground.push_back(&text);
		outline.setPrimitiveType(sf::LinesStrip);
		percentage = 0;
	}

	LoadBar::LoadBar(sf::Vector2f size, sf::Vector2f position, sf::Font *font,
			unsigned int fontsize, ColorPair colors)
		: Widget::Widget(size, position, font, fontsize, colors)
	{
		Background.push_back(&bg);
		Foreground.push_back(&outline);
		Foreground.push_back(&fg);
		Foreground.push_back(&text);
		outline.setPrimitiveType(sf::LinesStrip);
		percentage = 0;
		Refresh();
	}

	void LoadBar::updatePct(float pct)
	{
		percentage = pct;
		fg.setSize(sf::Vector2f(percentage * (Size.x - 10), Size.y - 10));
	}

	void LoadBar::Refresh()
	{
		bg.setPosition(Position);
		bg.setSize(Size);
		bg.setFillColor(Colors.Secondary);
		fg.setPosition(Position.x + 5, Position.y + 5);
		fg.setSize(sf::Vector2f(percentage * (Size.x - 10), Size.y - 10));
		fg.setFillColor(Colors.Primary);
		outline.resize(5);
		for (uint8_t i = 0; i < 5; i++)
		{
			outline[i].color = Colors.Primary;
		}
		sf::Vector2f p = sf::Vector2f(Position.x + 1, Position.y + 1);
		outline[0].position = p;
		outline[1].position = sf::Vector2f(p.x + Size.x - 2, p.y);
		outline[2].position = sf::Vector2f(p.x + Size.x - 2, p.y + Size.y - 2);
		outline[3].position = sf::Vector2f(p.x, p.y + Size.y - 2);
		outline[4].position = p;
	}
}
