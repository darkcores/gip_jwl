/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "../include/jwl/objects.h"

namespace jwl{
	sf::Color ColorPair_s::Primary_s;
	sf::Color ColorPair_s::Secondary_s;
	sf::VideoMode VideoSettings::Resolution;
	sf::Vector2f VideoSettings::Scale;

	ColorPair_s::ColorPair_s()
	{
		ColorPair_s::Primary_s = sf::Color::White;
		ColorPair_s::Secondary_s = sf::Color::Blue;
	}
	ColorPair_s::ColorPair_s(sf::Color PrimaryColor, sf::Color SecondaryColor)
	{
		ColorPair_s::Primary_s = PrimaryColor;
		ColorPair_s::Secondary_s = SecondaryColor;
	}
	ColorPair ColorPair_s::Colors()
	{
		ColorPair c;
		c.Primary = ColorPair_s::Primary_s;
		c.Secondary = ColorPair_s::Secondary_s;
		return c;
	}
	ColorPair ColorPair_s::inverted()
	{
		ColorPair c;
		c.Primary = ColorPair_s::Secondary_s;
		c.Secondary = ColorPair_s::Primary_s;
		return c;
	}

	ColorPair::ColorPair()
	{
		Primary = sf::Color::White;
		Secondary = sf::Color::Blue;
	}
	ColorPair::ColorPair(sf::Color PrimaryColor, sf::Color SecondaryColor)
	{
		Primary = PrimaryColor;
		Secondary = SecondaryColor;
	}
	ColorPair ColorPair::inverted()
	{
		ColorPair c;
		c.Primary = Secondary;
		c.Secondary = Primary;
		return c;
	}

	Action::Action()
	{
		size = nullptr;
		position = nullptr;
		object = nullptr;
	}
	Action::Action(sf::Vector2f *Size, sf::Vector2f *Position, void *Object)
	{
		size = Size;
		position = Position;
		object = Object;
	}
	VideoSettings::VideoSettings()
	{
		VideoSettings::Resolution = sf::VideoMode::getDesktopMode();
		VideoSettings::Scale = sf::Vector2f(Resolution.width / 32, Resolution.height / 16);
	}
}
