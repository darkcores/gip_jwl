/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "../include/jwl/widget.h"

namespace jwl{
	Widget::Widget()
	{
		Colors = ColorPair_s::Colors();
		Size = sf::Vector2f(0, 0);
		Position = sf::Vector2f(0, 0);
	}
	Widget::Widget(sf::Vector2f size, sf::Vector2f position, sf::Font *font,
			unsigned int fontsize)
	{
		Size = size;
		Position = position;
		Font = font;
		Colors = ColorPair_s::Colors();
		FontSize = fontsize;
	}
	Widget::Widget(sf::Vector2f size, sf::Vector2f position, sf::Font *font, 
			unsigned int fontsize, ColorPair colors)
	{
		Size = size;
		Position = position;
		Font = font;
		Colors = colors;
		FontSize = fontsize;
	}
	void Widget::handleEvent(Events e, void *param)
	{
		(void)e;
		(void)param;
	}
	void Widget::Refresh()
	{
	}
	Action Widget::getAction()
	{
		Action a;
		a.size = &Size;
		a.position = &Position;
		a.object = this;
		return a;
	}
	/**************************************************************************/
	sf::Vector2f Widget::lerp_pct(float pct)
	{
		return startvect + ((destvect - startvect) * pct);
	}
	ColorPair Widget::fade_pct(float pct)
	{
		ColorPair c;
		c.Primary.a = destcolor.Primary.a * pct;
		c.Primary.r = destcolor.Primary.r;
		c.Primary.g = destcolor.Primary.g;
		c.Primary.b = destcolor.Primary.b;
		c.Secondary.a = destcolor.Secondary.a * pct;
		c.Secondary.r = destcolor.Secondary.r;
		c.Secondary.g = destcolor.Secondary.g;
		c.Secondary.b = destcolor.Secondary.b;
		return c;
	}
	void Widget::MoveSet(sf::Vector2f start, sf::Vector2f dest)
	{
		startvect = start;
		destvect = dest;
		move = true;
	}
	void Widget::ColorSet(ColorPair dest)
	{
		destcolor = dest;
		color = true;
	}
	void Widget::Effect(float pct)
	{
		if(move)
			Position = lerp_pct(pct);
		if(color)
			Colors = fade_pct(pct);
		Refresh();
	}
	/**************************************************************************/
}
