/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "../include/jwl/window.h"

namespace jwl{
	Window::Window()
	{
		Overlay.push_back(&overlay);
		overlay.setFillColor(sf::Color(128, 128, 128, 64));
		Colors = ColorPair_s::Colors();
		selected = 0;
		FontSize = 20;
		border.setPrimitiveType(sf::PrimitiveType::LinesStrip);
		border.resize(5);
		for(int i = 0; i < 5; i++)
		{
			border[i].color = sf::Color(192, 192, 192);
		}
		Overlay.push_back(&border);
	}
	Window::Window(sf::Font *font, unsigned int fontsize)
	{
		Overlay.push_back(&overlay);
		overlay.setFillColor(sf::Color(128, 128, 128, 64));
		Colors = ColorPair_s::Colors();
		selected = 0;
		FontSize = fontsize;
		Font = font;
		border.setPrimitiveType(sf::PrimitiveType::LinesStrip);
		border.resize(5);
		for(int i = 0; i < 5; i++)
		{
			border[i].color = sf::Color(192, 192, 192);
		}
		Overlay.push_back(&border);
	}
	Window::Window(sf::Font *font, unsigned int fontsize, ColorPair colors)
	{
		Overlay.push_back(&overlay);
		overlay.setFillColor(sf::Color(128, 128, 128, 64));
		Font = font;
		Colors = colors;
		selected = 0;
		FontSize = fontsize;
		border.setPrimitiveType(sf::PrimitiveType::LinesStrip);
		border.resize(5);
		for(int i = 0; i < 5; i++)
		{
			border[i].color = sf::Color(192, 192, 192);
		}
		Overlay.push_back(&border);
	}
	void Window::Refresh()
	{
	}
	void Window::LoadFunc()
	{
	}
	void Window::EndFunc()
	{
	}
	int Window::testAction(std::vector<Action> &actions, sf::Vector2f &mpos)
	{
		for(unsigned int i = 0; i < actions.size(); i++)
		{
			if(mpos.x > actions[i].position->x &&
				mpos.x < actions[i].position->x + actions[i].size->x &&
				mpos.y > actions[i].position->y && 
				mpos.y < actions[i].position->y + actions[i].size->y)
			{
				return i;
			}
		}
		return -1;
	}
	int Window::testHover(std::vector<Action> &actions, sf::Vector2i &mpos)
	{
		for(unsigned int i = 0; i < actions.size(); i++)
		{
			if(mpos.x > actions[i].position->x &&
				mpos.x < actions[i].position->x + actions[i].size->x &&
				mpos.y > actions[i].position->y && 
				mpos.y < actions[i].position->y + actions[i].size->y)
			{
				return i;
			}
		}
		return -1;
	}
	void Window::Run(sf::RenderWindow *window)
	{
		sf::Event event, m;
		isRunning = true;
		Widget *w;
		win = window;
		LoadFunc();
		window->pollEvent(m);
		w = reinterpret_cast<Widget*>(Actions[selected].object);
		do {
			do {
				window->pollEvent(event);
			}while(event.type == m.type && event.type != sf::Event::MouseMoved);
			m = event;
			switch(event.type)
			{
				case sf::Event::MouseMoved:
					SetHover();//een test of aparte thread nodig is
					break;
				case sf::Event::MouseButtonReleased:
					if(event.mouseButton.button == sf::Mouse::Button::Left)
					{
						sf::Vector2f mpos = sf::Vector2f(event.mouseButton.x, event.mouseButton.y);
						int tmp = testAction(Actions, mpos);
						if(tmp != -1)
						{
							w = reinterpret_cast<Widget*>(Actions[tmp].object);
							w->handleEvent(Events::Click, &mpos);
							selected = tmp;
							Click();
							SetBorder();
						}
					}
					break;
				case sf::Event::MouseWheelMoved:
					if(event.mouseWheel.delta > 0)
						w->handleEvent(Events::Up, nullptr);
					else
						w->handleEvent(Events::Down, nullptr);
					break;
				case sf::Event::TextEntered:
					if(event.text.unicode < 128)
					{
						w->handleEvent(Events::Char, &event.text.unicode);
					}
					break;
				case sf::Event::KeyReleased:
						switch(event.key.code)
						{
							{
								case sf::Keyboard::Return:
									sf::Vector2f tmp(0, 0);
									w->handleEvent(Events::Click, &tmp);
									Click();
									SetBorder();
									break;
							}
							{
								case sf::Keyboard::Up:
									w->handleEvent(Events::Up, nullptr);
									break;
							}
							{
								case sf::Keyboard::Down:
									w->handleEvent(Events::Down, nullptr);
									break;
							}
							{
								case sf::Keyboard::Left:
									w->handleEvent(Events::Left, nullptr);
									break;
							}
							{
								case sf::Keyboard::Right:
									w->handleEvent(Events::Right, nullptr);
									break;
							}
							{
								default:
									break;
							}
						}
					break;
				case sf::Event::Closed:
					isRunning = false;
					break;
				case sf::Event::Resized:
					//TODO voor resize bij windowed maybe?
					break;
				case sf::Event::LostFocus:
					break;
				default:
					break;
			}
		}while(isRunning);
		EndFunc();
	}
	void Window::SetHover()
	{
		sf::Vector2i mpos = sf::Mouse::getPosition(*win);
		int test = testHover(Hover, mpos);
		if(test != -1)
		{
			overlay.setPosition(Hover[test].position->x, Hover[test].position->y);
			overlay.setSize(*Hover[test].size);
		}
		else
		{
			overlay.setPosition(-2, -2);
			overlay.setSize(sf::Vector2f(1, 1));
		}
	}
	void Window::SetBorder()
	{
		border[0].position = sf::Vector2f(Actions[selected].position->x + 2,
				Actions[selected].position->y + 2);
		border[1].position = sf::Vector2f(Actions[selected].position->x + 
				Actions[selected].size->x - 2, Actions[selected].position->y + 2);
		border[2].position = sf::Vector2f(Actions[selected].position->x + 
				Actions[selected].size->x - 2, Actions[selected].position->y + 
				Actions[selected].size->y - 2);
		border[3].position = sf::Vector2f(Actions[selected].position->x + 2,
				Actions[selected].position->y + Actions[selected].size->y - 2);
		border[4].position = border[0].position;
	}
	void Window::Click()
	{
	}
}
