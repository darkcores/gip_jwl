/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of The Jorrit Widget Library
 * 
 * The Jorrit Widget Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Jorrit Widget Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with The Jorrit Widget Library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "../include/jwl/table.h"

namespace jwl{
	static int table_callback(void *obj, int cols, char **rowstrings, char **colnames)
	{
		Table *tbl = reinterpret_cast<Table*>(obj);
		return tbl->callback(cols, rowstrings, colnames);
	}
	Table::Table() : Widget::Widget()
	{
		selected = 1;
		startlst = 0;
		listmode = false;
		Background.push_back(&back);
		Background.push_back(&sbar);
		Background.push_back(&sslider);
		Background.push_back(&overlay);
		Background.push_back(&grid);
		grid.setPrimitiveType(sf::PrimitiveType::Lines);
		custom_width =false;
		widths = NULL;
	}
	Table::Table(sf::Vector2i tbls, sf::Font *font, unsigned int fontsize,
			sf::Vector2f size, sf::Vector2f position, ColorPair colors)
		: Widget::Widget(size, position, font, fontsize, colors)
	{
		tablesize = tbls;
		selected = 1;
		startlst = 0;
		listmode = false;
		Background.push_back(&back);
		Background.push_back(&sbar);
		Background.push_back(&sslider);
		Background.push_back(&overlay);
		Background.push_back(&grid);
		grid.setPrimitiveType(sf::PrimitiveType::Lines);
		custom_width =false;
		widths = NULL;
		Refresh();
	}
	Table::Table(int rows, sf::Font *font, unsigned int fontsize,
			sf::Vector2f size, sf::Vector2f position, ColorPair colors, bool listMode)
		: Widget::Widget(size, position, font, fontsize, colors)
	{
		tablesize = sf::Vector2i(1, rows);
		if(listMode)
			selected = 0;
		else
			selected = 1;
		startlst = 0;
		listmode = listMode;
		Background.push_back(&back);
		Background.push_back(&sbar);
		Background.push_back(&sslider);
		Background.push_back(&overlay);
		Background.push_back(&grid);
		grid.setPrimitiveType(sf::PrimitiveType::Lines);
		custom_width =false;
		widths = NULL;
		Refresh();
	}
	void Table::Refresh()
	{
		Foreground.clear();
		txtlst.clear();
		txtlst.resize(tablesize.x * tablesize.y);
		float startx = Position.x + 10;
		float starty = Position.y + 10;
		if(custom_width)
		{
			float stepx = startx;
			float stepy = (Size.y-10)/tablesize.y;
			for (int i = 0; i < tablesize.x * tablesize.y; i++)
			{
				if(i%tablesize.x == 0)
					stepx = startx;
				stepx += widths[(int)(i%tablesize.x) - 1];
				txtlst[i].setColor(Colors.Secondary);
				txtlst[i].setFont(*Font);
				txtlst[i].setCharacterSize(FontSize);
				txtlst[i].setPosition(stepx, starty + i/tablesize.x * stepy);
				Foreground.push_back(&txtlst[i]);
			}
		}
		else
		{
			float stepx = (Size.x-20)/tablesize.x;
			float stepy = (Size.y-10)/tablesize.y;
			for (int i = 0; i < tablesize.x * tablesize.y; i++)
			{
				txtlst[i].setColor(Colors.Secondary);
				txtlst[i].setFont(*Font);
				txtlst[i].setCharacterSize(FontSize);
				txtlst[i].setPosition(startx + i%tablesize.x * stepx, starty + i/tablesize.x * stepy);
				Foreground.push_back(&txtlst[i]);
			}
		}
		back.setFillColor(Colors.Primary);
		back.setPosition(Position.x, Position.y);
		back.setSize(Size);
		sbar.setFillColor(Colors.Secondary);
		sbar.setPosition(Position.x + Size.x - 12, Position.y + 5);
		sbar.setSize(sf::Vector2f(9, Size.y - 10));
		sslider.setFillColor(Colors.Primary);
		sslider.setSize(sf::Vector2f(5, 15));
		overlay.setFillColor(sf::Color(128, 128, 128, 32));
		overlay.setSize(sf::Vector2f(Size.x - 20, (Size.y - 20) / tablesize.y));

		generateGrid();
		updateText();
		updateScrollbar();
		updateOverlay();
	}
	void Table::setWidth(unsigned char column, float width)
	{
		if(widths == 0)
			widths = new float[tablesize.x];
		custom_width = true;
		widths[column] = width;
		if(column == tablesize.x -1)
			Refresh();
	}
	void Table::QuickRefresh()
	{
		updateText();
		updateScrollbar();
		updateOverlay();
	}
	void Table::generateGrid()
	{
		int gridsize = 2*(tablesize.x+1) + 2*(tablesize.y+1);
		int curr = 0;
		grid.resize(gridsize);
		float start, step;
		//horizontal
		start = Position.y + 5;
		step = (Size.y-10)/tablesize.y;
		for (unsigned char i = 0; i <= tablesize.y; i++)
		{
			grid[curr].color = Colors.Secondary;
			grid[curr].position = sf::Vector2f(Position.x+5, start+step*i);
			curr++;
			grid[curr].color = Colors.Secondary;
			grid[curr].position = sf::Vector2f(Position.x+Size.x-15, start+step*i);
			curr++;
		}
		if(custom_width)
		{
			//vertical
			start = Position.x + 5;
			step = start;
			for(unsigned char i = 0; i <= tablesize.x; i++)
			{
				if(i > 0)
					step += widths[i-1];
				grid[curr].color = Colors.Secondary;
				grid[curr].position = sf::Vector2f(step, Position.y+5);
				curr++;
				grid[curr].color = Colors.Secondary;
				grid[curr].position = sf::Vector2f(step, Position.y+Size.y-5);
				curr++;
			}
		}
		else
		{
			//vertical
			start = Position.x + 5;
			step = (Size.x-20)/tablesize.x;
			for(unsigned char i = 0; i <= tablesize.x; i++)
			{
				grid[curr].color = Colors.Secondary;
				grid[curr].position = sf::Vector2f(start+step*i, Position.y+5);
				curr++;
				grid[curr].color = Colors.Secondary;
				grid[curr].position = sf::Vector2f(start+step*i, Position.y+Size.y-5);
				curr++;
			}
		}
	}
	void Table::updateText()
	{
		if(lst.size() == 0)
			return;
		unsigned int yc;
		if(lst.size() > (unsigned int) tablesize.y) 
			yc = tablesize.y;
		else
			yc = lst.size();
		if(!listmode)
		{
			for (int i = 0; i < tablesize.x; i++) {
				txtlst[i].setString(lst[0][i]);
				txtlst[i].setStyle(sf::Text::Bold);
			}
			for (unsigned int i = 1 + startlst; i < yc+startlst; i++)
			{
				for (int j = 0; j < tablesize.x; j++)
				{
					txtlst[(i-startlst)*tablesize.x+j].setString(lst[i][j]);
				}
			}
			for (int i = yc+startlst; i < tablesize.y; i++)
			{
				for (int j = 0; j < tablesize.x; j++)
				{
					txtlst[(i-startlst)*tablesize.x+j].setString("");
				}
			}
		}
		else
		{
			for (unsigned int i = 0 + startlst; i < yc+startlst; i++)
			{
				for (int j = 0; j < tablesize.x; j++)
				{
					txtlst[(i-startlst)*tablesize.x+j].setString(lst[i][j]);
				}
			}
		}
	}
	void Table::updateScrollbar()
	{
		int pos = Position.y + 7;
		pos += Size.y / lst.size() * selected;
		sslider.setPosition(Position.x + Size.x - 10, pos);
	}
	void Table::updateOverlay()
	{
		overlay.setPosition(Position.x + 5,
				Position.y + 5 + (selected - startlst) * (Size.y-10) / tablesize.y);
	}
	void Table::handleEvent(Events e, void *param)
	{
		sf::Vector2f *mpos;
		switch(e)
		{
			case Events::Up:
				if(listmode)
				{
					if(selected > 0)
						selected--;
				}
				else
				{
					if (selected > 1)
						selected--;
				}
				if (selected - startlst < 1)
					startlst--;
				QuickRefresh();
				break;
			case Events::Down:
				if (selected < lst.size() - 1)
				{
					selected++;
					if (selected - startlst > (unsigned int)tablesize.y - 3 &&
							lst.size() - startlst > (unsigned int)tablesize.y)
						startlst++;
				}
				QuickRefresh();
				break;
			case Events::Click:
				mpos = reinterpret_cast<sf::Vector2f *>(param);
				if(listmode)
				{
					if(mpos->y > Position.y + 5 && mpos->y < Position.y + Size.y - 5)
					{
						float p = mpos->y - Position.y - 5;
						unsigned int s;
						s = startlst + (p / (Size.y / tablesize.y));
						if (s < lst.size())
							selected = s;
						QuickRefresh();
					}
				}
				else
				{
					if(mpos->y > Position.y + 5 + (Size.y / tablesize.y) && 
							mpos->y < Position.y + Size.y - 5)
					{
						float p = mpos->y - Position.y - 5;
						unsigned int s;
						s = startlst + (p / (Size.y / tablesize.y));
						if (s < lst.size())
							selected = s;
						QuickRefresh();
					}
				}
				break;
			default:
				break;
		}
	}
	void Table::clear()
	{
		lst.clear();
		startlst = 0;
		if(listmode)
			selected = 0;
		else
			selected = 1;
		for (int i = 0; i < tablesize.x * tablesize.y; i++)
		{
			txtlst[i].setString("");
		}
		QuickRefresh();
	}
	void Table::setDataSource(const char *file, const char *query)
	{
		sqlite3_open(file, &db);
		sqlite3_exec(db, query, table_callback, this, NULL);//char ptr voor error is die NULL
		sqlite3_close(db);
	}
	int Table::callback(int cols, char **rowstrings, char **colnames)
	{
		tablesize.x = cols;
		tablesize.y = 10;
		lst.clear();
		for (int i = 0; i < cols; i++)
		{
			txtlst[i].setString(colnames[i]);
		}
		std::vector< std::string > item;
		item.resize(cols);
		for(int i = 0; i < cols; i++)
		{
			item[i] = rowstrings[i];
		}
		lst.push_back(item);
		Refresh();
		return 0;
	}
}

