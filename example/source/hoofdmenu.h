/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef HOOFDMENU_H
#define HOOFDMENU_H

#include "../../include/objects.h"
#include "../../include/window.h"
#include "../../include/button.h"
#include "../../include/textbox.h"
#include "../../include/messagebox.h"
#include "../../include/filedialog.h"
#include "../../include/table.h"
#include "../../include/loadbar.h"
#include "../../include/checkbox.h"

#include "settings.h"

/**
 * Hoofdmenu van het programma. \n
 */
class Hoofdmenu : public jwl::Window
{
	private:
		sf::Texture logo, bg;
		sf::Sprite biglogo, logospr, background;
		std::vector< sf::VertexArray > driehoeken;

		//knoppen
		jwl::Button *sluiten, *over, *bestanden, *aanpassen, *verwerken, *weergeven, *statistieken;

		//messagebox
		jwl::Messagebox *msgsluiten;

		//test objecten
		jwl::Textbox *testxt;

		jwl::Table *testtbl;
		jwl::Filedialog *testfile;
		jwl::LoadBar *lbar;
		jwl::Checkbox *chkbox;
	public:
		/**
		 * Constructor met font, fontsize en kleurenpaar. \n
		 */
		Hoofdmenu(sf::Font*, unsigned int, jwl::ColorPair);
		~Hoofdmenu();

		virtual void Click();
		virtual void LoadFunc();
};

#endif
