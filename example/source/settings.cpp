/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "settings.h"

/**
 * Variabelen initialiseren
 */
sf::VideoMode Settings::resolution;
unsigned int Settings::AA;
bool Settings::vsync;
unsigned int Settings::framelimit;
sf::Vector2f Settings::scale;
std::string Settings::fontlocation;
bool Settings::debug;
bool Settings::fullscreen;

Settings::Settings()
{
	Settings::resolution = sf::VideoMode::getDesktopMode();
	Settings::AA = 0;
	Settings::vsync = true;
	Settings::framelimit = 0;
	Settings::scale = sf::Vector2f(resolution.width / 32, resolution.height / 18);
	Settings::fontlocation = "AftaSansThin-Regular.otf";
	Settings::debug = false;
	Settings::fullscreen = true;
}

Settings::~Settings()
{
}
