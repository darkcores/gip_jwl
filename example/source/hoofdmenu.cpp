/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "hoofdmenu.h"

/**
 * Lijst met knoppen. voor niet 0, 1, ... te hoeven gebruiken.
 */
enum KNOPPEN {
	SLUITEN,
	BESTANDEN,
	AANPASSEN,
	VERWERKEN,
	WEERGEVEN,
	STATISTIEKEN,
	OVER
};

Hoofdmenu::Hoofdmenu(sf::Font *font, unsigned int fontsize, jwl::ColorPair colors)
	: Window::Window(font, fontsize, colors)
{
	/* logo */
	bg.loadFromFile("background.png");
	bg.setSmooth(true);
	background.setTexture(bg);
	background.setPosition(0, 0);
	background.setScale(Settings::scale.x / 80, Settings::scale.y / 85);
	Background.push_back(&background);
	
	logo.loadFromFile("logo.png");
	logo.setSmooth(true);
	logospr.setTexture(logo);
	logospr.setPosition(25 * Settings::scale.x, 14 * Settings::scale.y);
	logospr.setScale(Settings::scale.x / 150, Settings::scale.y / 150);
	biglogo.setTexture(logo);
	biglogo.setPosition(10 * Settings::scale.x, 3 * Settings::scale.y);
	biglogo.setScale(Settings::scale.x / 50, Settings::scale.y / 50);
	Background.push_back(&logospr);
	Background.push_back(&biglogo);

	/* knoppen */
	sluiten = new jwl::Button("Sluiten", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(5*Settings::scale.x, Settings::scale.y),
				Colors.inverted());
	Foreground.push_back(sluiten);
	Actions.push_back(sluiten->getAction());

	over = new jwl::Button("Filedialog", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
	Settings::scale.y), sf::Vector2f(5*Settings::scale.x, Settings::scale.y*5), Colors);
	Foreground.push_back(over);
	Actions.push_back(over->getAction());
	Hover.push_back(over->getAction());

	testxt = new jwl::Textbox(Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(15*Settings::scale.x, Settings::scale.y), Colors);
	Foreground.push_back(testxt);
	Actions.push_back(testxt->getAction());

	Hover.push_back(sluiten->getAction());

	chkbox = new jwl::Checkbox("Check", Font, FontSize, sf::Vector2f(Settings::scale.x*5,
				Settings::scale.y), sf::Vector2f(15*Settings::scale.x, Settings::scale.y*5),
				Colors, false);
	Foreground.push_back(chkbox);
	Actions.push_back(chkbox->getAction());
	Hover.push_back(chkbox->getAction());

	lbar = new jwl::LoadBar(sf::Vector2f(Settings::scale.x*10, Settings::scale.y * 1),
				sf::Vector2f(Settings::scale.x*3, Settings::scale.y*10), Font, FontSize,
				Colors.inverted());
	Foreground.push_back(lbar);
	lbar->updatePct(0.30);

	//table
	
	testtbl = new jwl::Table(sf::Vector2i(3, 8), Font, FontSize, sf::Vector2f(500, 500),
				sf::Vector2f(1000, 300), Colors);
	std::vector< std::string > lstitem(3), lstitem2(3);
	lstitem[0] = "efeffe";
	lstitem[1] = "efeffe";
	lstitem[2] = "efeffe";
	lstitem2[0] = "uyjjfe";
	lstitem2[1] = "ujyuje";
	lstitem2[2] = "yujuye";
	for (int i = 0; i < 10; i++) 
	{
		testtbl->lst.push_back(lstitem);
		testtbl->lst.push_back(lstitem2);
	}
	testtbl->QuickRefresh();
	Actions.push_back(testtbl->getAction());
	Foreground.push_back(testtbl);
}

Hoofdmenu::~Hoofdmenu()
{
	delete sluiten;
	delete over;
	//delete testtbl;
	delete chkbox;
	delete lbar;
	delete testxt;
}

void Hoofdmenu::Click()
{
	switch(selected)
	{
		case KNOPPEN::SLUITEN:
			msgsluiten = new jwl::Messagebox(Font, FontSize, Colors.inverted());
			msgsluiten->Text.setString("Wilt u sluiten?");
			Overlay.push_back(msgsluiten);
			msgsluiten->Run(win);
			if(msgsluiten->result)
				isRunning = false;
			Overlay.pop_back();
			delete msgsluiten;
			break;
		case KNOPPEN::BESTANDEN:
			testfile = new jwl::Filedialog(Font, FontSize / 3 * 2, Colors.inverted());
			Overlay.push_back(testfile);
			testfile->Run(win);
			Overlay.pop_back();
			delete testfile;
			break;
		case KNOPPEN::AANPASSEN:
			break;
		case KNOPPEN::VERWERKEN:
			break;
		case KNOPPEN::WEERGEVEN:
			break;
		case KNOPPEN::STATISTIEKEN:
			break;
		case OVER:
			break;
		default:
			break;
	}
}

void Hoofdmenu::LoadFunc()
{
	sluiten->MoveSet(sf::Vector2f(0, 0), sf::Vector2f(100, 100));
	sluiten->ColorSet(Colors.inverted());
	for (float i = 0.00; i <= 1.00; i += 0.01)
	{
		sluiten->Effect(i);
		sf::sleep(sf::milliseconds(50));
	}
}
