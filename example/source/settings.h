/* 
 * Written by Gerets Jorrit <gerets.jorrit@telenet.be>
 *
 * This file is part of GIP 2015 DOE DAG
 * 
 * GIP 2015 DOE DAG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GIP 2015 DOE DAG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GIP 2015 DOE DAG.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SETTINGS_H
#define SETTINGS_H

#include <SFML/Graphics.hpp>
#include <atomic>
 
 /**
 * Class met de instellingen. \n
 */
class Settings
{
	public:
		/**
		 * Constructor. \n
		 */
		Settings();
		/**
		 * Destructor. \n
		 */
		~Settings();

		/**
		 * De resolutie van het venster. \n
		 */
		static sf::VideoMode resolution;
		/**
		 * Anti Aliasing installingen 0 - 16. \n
		 * 0 is uit
		 */
		static unsigned int AA;
		/**
		 * Vertical sync. \n
		 * Werkt niet met framerate limit
		 */
		static bool vsync;
		/**
		 * Limieteer het maximum frames / seconde. \n
		 * 0 is uit
		 */
		static unsigned int framelimit;
		/**
		 * Variabele voor UI schaal. \n
		 */
		static sf::Vector2f scale;
		/**
		 * String with the location of the font file. \n
		 */
		static std::string fontlocation;
		/**
		 * Toon debug info op het scherm. \n
		 */
		static bool debug;
		/**
		 * Is het venster fullescreen. \n
		 */
		static bool fullscreen;
};
#endif
