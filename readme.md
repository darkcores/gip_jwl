# JWL Library #

Widget library for my GIP project.

## Features ##

* Flat and simple UI
* Common objects like buttons, textboxes, ...
* Written with SFML, all objects inherit from SFML::Drawable

## Compiling ##

How to compile the library:

	git clone https://bitbucket.org/darkcores/GIP_jwl
	cd GIP_jwl
	make

## Example ##
	
This is just an example program using the library.

If you have not yet compiled the library do this first:
	
	git clone https://bitbucket.org/darkcores/GIP_jwl
	cd GIP_jwl
	make

Compiling the example:

	cd example
	make
	./testproj
	
